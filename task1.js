const jsonfile = require("jsonfile");
const randomstring = require("randomstring");

const inputFile = "input2.json";
const outputFile = "output2.json";

var output = {}
jsonfile.readFile(inputFile, function(err, body){
    const emails = body.names.map(name => {
        let email = '';
        for(let i = name.length-1; i >= 0; i-- ){
            email += name[i];
        }
        email += randomstring.generate(5) + '@gmail.com';
        return email;
    })
    output.emails = emails;
    console.log("saving output file formatted with 2 space indenting");
    jsonfile.writeFile(outputFile, output, {spaces: 2}, function(err) {
      console.log("All done!");
    });
});
